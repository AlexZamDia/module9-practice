terraform {
  required_providers {
    yandex = {
      source = "yandex-cloud/yandex"
    }
  }
  required_version = ">= 0.13"
}

provider "yandex" {
  zone = "ru-central1-a"
}

# Получаем идентификатор последнего образа Ubuntu
data "yandex_compute_image" "last_ubuntu" {
  family = "ubuntu-2204-lts" # Ubuntu 22.04 LTS
}

# Найдем сеть по-умолчанию вместо создания новой
data "yandex_vpc_subnet" "default_a" {
  name = "default-ru-central1-a" # одна из дефолтных подсетей
}



resource "yandex_compute_instance" "web1" {
  name = "web1"
  #platform_id = "standard-v1" # тип процессора (Intel Broadwell)

  resources {
    cores  = 2
    memory = 2
  }

  boot_disk {
    initialize_params {
      #image_id = "fd8emvfmfoaordspe1jr" # Ubuntu 22.04 LTS
      image_id = data.yandex_compute_image.last_ubuntu.id
      size     = "8"
    }
  }

  network_interface {
    #subnet_id = yandex_vpc_subnet.subnet-1.id
    subnet_id = data.yandex_vpc_subnet.default_a.subnet_id
    nat       = true
  }

  metadata = {
    ssh-keys  = "ubuntu:${file("~/.ssh/id_ed25519.pub")}"
    user-data = "${file("user_data.sh")}"
  }
}

resource "yandex_compute_instance" "web2" {
  name = "web2"
  #platform_id = "standard-v1" # тип процессора (Intel Broadwell)

  resources {
    cores  = 2
    memory = 2
  }

  boot_disk {
    initialize_params {
      #image_id = "fd8emvfmfoaordspe1jr" # Ubuntu 22.04 LTS
      image_id = data.yandex_compute_image.last_ubuntu.id
      size     = "8"
    }
  }

  network_interface {
    #subnet_id = yandex_vpc_subnet.subnet-1.id
    subnet_id = data.yandex_vpc_subnet.default_a.subnet_id
    nat       = true
  }

  metadata = {
    ssh-keys  = "ubuntu:${file("~/.ssh/id_ed25519.pub")}"
    user-data = "${file("user_data.sh")}"
  }
}

resource "yandex_compute_instance" "react" {
  name = "react"
  #platform_id = "standard-v1" # тип процессора (Intel Broadwell)

  resources {
    cores  = 2
    memory = 2
  }

  boot_disk {
    initialize_params {
      #image_id = "fd8emvfmfoaordspe1jr" # Ubuntu 22.04 LTS
      image_id = data.yandex_compute_image.last_ubuntu.id
      size     = "8"
    }
  }

  network_interface {
    #subnet_id = yandex_vpc_subnet.subnet-1.id
    subnet_id = data.yandex_vpc_subnet.default_a.subnet_id
    nat       = true
  }

  metadata = {
    ssh-keys  = "ubuntu:${file("~/.ssh/id_ed25519.pub")}"
  }
}


resource "yandex_lb_target_group" "web-servers" {
  name = "web-servers-target-group"
 
  target {
    subnet_id = data.yandex_vpc_subnet.default_a.subnet_id
    address   = yandex_compute_instance.web1.network_interface.0.ip_address
  }
 
  target {
    subnet_id = data.yandex_vpc_subnet.default_a.subnet_id
    address   = yandex_compute_instance.web2.network_interface.0.ip_address
  }
}


resource "yandex_lb_network_load_balancer" "lbweb" {
  name = "lbweb"

  listener {
    name = "listener-web-servers"
    port = 80
    external_address_spec {
      ip_version = "ipv4"
    }
  }

  attached_target_group {
    target_group_id = yandex_lb_target_group.web-servers.id

    healthcheck {
      name = "http"
      http_options {
        port = 80
        path = "/"
      }
    }
  }
}

output "internal_ip_address_web1" {
  description = "web1 ip"
  value = yandex_compute_instance.web1.network_interface.0.ip_address
}

output "external_ip_address_web1" {
  description = "web1 ip NAT"
  value = yandex_compute_instance.web1.network_interface.0.nat_ip_address
}

output "internal_ip_address_web2" {
  description = "web2 ip"
  value = yandex_compute_instance.web2.network_interface.0.ip_address
}

output "external_ip_address_web2" {
  description = "web2 ip NAT"
  value = yandex_compute_instance.web2.network_interface.0.nat_ip_address
}

output "lb_ip_address" {
  value = yandex_lb_network_load_balancer.lbweb.*
}

output "internal_ip_address_react" {
  description = "Elatic IP address assigned to our ReactJS Server"
  value = yandex_compute_instance.react.network_interface.0.ip_address
}

output "external_ip_address_react" {
  description = "Elatic IP address assigned to our ReactJS Server NAT"
  value = yandex_compute_instance.react.network_interface.0.nat_ip_address
}